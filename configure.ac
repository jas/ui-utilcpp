AC_INIT([ui-utilcpp], [1.10.0])

dnl See DEVELOPERS file for shared library versioning instructions
dnl INTERFACE:REVISION:AGE
dnl One of A, B1 or B2 will be your path:
dnl
dnl  A) If only the source code, but not the interface was changed,
dnl     the REVISION is increased by one.
dnl  B) If the interface changed, the INTERFACE number is increased by
dnl     one and REVISION is set to zero.
dnl     B1) If only new interfaces were added, the AGE is increased by
dnl         one.
dnl     B2) If interfaces were removed or there were incompatible
dnl         changes, the AGE is set to zero.
UI_INIT([src/ui-utilcpp/Exception.hpp], [9:1:0],,,[tar-pax])
AC_CONFIG_HEADER(config.h)

UI_DOXYGEN
UI_DISTDIR_CRUFT

# Checks for programs.
UI_PROG_CXX
AC_PROG_CXXCPP
AC_PROG_INSTALL
AC_PROG_LN_S
AC_PROG_MAKE_SET
LT_INIT
AC_PROG_AWK

# Checks for libraries.

# IMPORTANT: Do not make standard library checks here -- i.e. no
# checks that would add the checked library to "LIBS" (unless it is
# actually common to all sub-libraries), as we produce sub-libraries
# with varying dependencies.

# check for dl lib
# All symbolic libs are linked with
AC_LANG_PUSH(C++)
AC_SYS_LARGEFILE
AC_CHECK_HEADER(dlfcn.h,, [ AC_MSG_ERROR(error: libdl include file dlfcn.h not found.)])
AC_CHECK_LIB(dl, dlopen,, [ AC_MSG_ERROR(Failed to detect libdl.)])

# check for ucommon
AC_CHECK_LIB(ucommon,   _ZTTN7ucommon6StringE,,    [ AC_MSG_ERROR(Failed to detect libucommon.)])
AC_CHECK_LIB(usecure,   _ZN7ucommon6Random3getEv,, [ AC_MSG_ERROR(Failed to detect libusecure.)])
AC_CHECK_LIB(commoncpp, _ZN3ost10DCCPSocketD0Ev,,  [ AC_MSG_ERROR(Failed to detect libcommoncpp.)])

# Normally, the dependency of libreadline to ncurses should be implicit and should
# be encapsulated - we do not use anything from ncurses explicitely.
# However, on some systems (at least for RH 7.3) this is not the case, so we need to check for ncurses, too
AC_CHECK_LIB(ncurses, curses_version,,  [ AC_MSG_ERROR(ui-utilcpp needs ncurses.) ])
AC_CHECK_LIB(cap, cap_init,,            [ AC_MSG_ERROR(ui-utilcpp needs libcap.) ])
AC_CHECK_LIB(readline, readline,,       [ AC_MSG_ERROR(ui-utilcpp needs libreadline.) ], -lncurses)
AC_CHECK_LIB(recode, recode_new_outer,, [ AC_MSG_ERROR(ui-utilcpp needs librecode.) ])
AC_CHECK_LIB(idn, idna_to_ascii_8z,, [ AC_MSG_ERROR(ui-utilcpp needs libidn.) ])
AC_CHECK_HEADER(idna.h, , [ AC_MSG_ERROR("Header error: idna.h")])

dnl Enable libproc
AC_ARG_ENABLE(libproc,
              [  --enable-libproc        Use libproc for procfs parsing (has memory leak issues). ],
              [ AC_CHECK_LIB(proc, openproc,, [ AC_MSG_ERROR(ui-utilcpp needs libproc.) ])
                AC_CHECK_HEADER(proc/readproc.h, , [ AC_MSG_ERROR("Header error: proc/readproc.h")]) ])

# boost unit test framework check
AC_CHECK_HEADER(boost/test/auto_unit_test.hpp, , [ AC_MSG_ERROR("Header error: boost/test/auto_unit_test.hpp")])
AC_CHECK_HEADER(boost/serialization/access.hpp, , [ AC_MSG_ERROR("Header error: boost/serialization/access.hpp")])
AC_CHECK_HEADER(boost/multi_index_container.hpp, , [ AC_MSG_ERROR("Header error: boost/multi_index_container.hpp")])
AC_CHECK_HEADER(boost/date_time/date.hpp, , [ AC_MSG_ERROR("Header error: boost/date_time/date.hpp")])
AC_CHECK_HEADER(boost/random.hpp, , [ AC_MSG_ERROR("Header error: boost/random.hpp")])
# Test for library missing (unfortunately, this does not seem to be trivial)

# Quota support (std and xfs only, 26 support is build-in)
AC_CHECK_HEADERS([sys/quota.h], [], [AC_MSG_WARN(No linux "standard" quota available!)])
AC_CHECK_HEADERS([xfs/xqm.h],   [], [AC_MSG_WARN(No linux "xfs" quota available!)])

# Checks for header files.
AC_HEADER_STDC

# Functuonal tests.
AC_FUNC_STRERROR_R
AC_CHECK_FUNCS(cap_compare)
AC_CHECK_FUNCS(cap_clear_flag)

# Push generic flags
AC_SUBST(AM_CPPFLAGS, ["-I\$(top_srcdir)/src"])
AC_SUBST(AM_CXXFLAGS, ["-pthread -lrpcsvc $(ucommon-config --cflags)"])
AC_SUBST(AM_LDFLAGS, ["-pthread"])
AC_SUBST(AM_LIBADD, $(ucommon-config --libs))
AC_SUBST(AM_LDADD, $(ucommon-config --libs))

# Local options
AC_ARG_ENABLE(strerror,
              [  --disable-strerror      Disable usage of strerror(3) (avoid segfaults w/ static/wrong libc). ],
              [ AM_CPPFLAGS+=" -DUI_UTILCPP_DISABLE_STRERROR" ])

AC_LANG_POP

# Harvest
AC_CONFIG_FILES([
	Makefile
	Doxyfile
	doc/Makefile
	src/Makefile
	src/tools/Makefile
	src/ui-utilcpp/Makefile
	src/ui-utilcpp/http/Makefile
])
AC_OUTPUT
