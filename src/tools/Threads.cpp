// Local
#include "config.h"

// STDC++
#include <iostream>
#include <fstream>
#include <vector>

// POSIX C

// C++ Libraries
#include <ui-utilcpp/Time.hpp>
#include <ui-utilcpp/Thread.hpp>

template <typename T>
class MyThread: public T
{
private:
	int run()
	{
		for (int i(0); i < 5; ++i)
		{
			std::cout << "(PID " << T::getPID() << ": blurp)..." << std::flush;
			UI::Util::nssleep(1);
		}
		return(42);
	}
};

void test(UI::Util::ProcessThread * t)
{
	std::cout << "Starting thread: " << std::flush;
	t->start();
	int exitCode(t->wait());
	std::cout << "finished with status: " << exitCode << std::endl;
}

int main()
{
	try
	{
		MyThread<UI::Util::ForkThread> ft;
		test(&ft);
	}
	catch(...)
	{
		std::cerr << "Exception" << std::endl;
	}
}
