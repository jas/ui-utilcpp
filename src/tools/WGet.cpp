// Local configuration
#include "config.h"

// STDC++
#include <iostream>
#include <exception>

// C++ Libraries
#include <ui-utilcpp/Exception.hpp>
#include <ui-utilcpp/Http.hpp>

int main(int argc, char *argv[])
{
#ifdef WIN32
	UI::Util::Sys::wsaStartup();
#endif

	int retVal(0);
	try
	{
		if (argc < 2) throw UI::Exception("Wrong usage");
		UI::Util::Http::URLGet get(argv[1]);
		std::cout << get.getStatus().get() << std::endl;
		std::cout << get.getHeader().get() << std::endl;
		std::cout << get.getBody() << std::endl;
	}
	catch (std::exception const & e)
	{
		std::cerr << "Exception: " << e.what() << "." << std::endl;
		std::cerr << "Usage: ui-utilcpp-wget URL" << std::endl;
		retVal = 1;
	}

#ifdef WIN32
	UI::Util::Sys::wsaCleanup();
#endif
	return retVal;
}
