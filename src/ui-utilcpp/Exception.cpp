#include "config.h"

// Implementation
#include "Exception.hpp"

// STDC++
#include <typeinfo>

namespace UI {

std::string const Exception::Errno_       ("xxxERRNOxxx");
std::string const Exception::NoWhatGiven_ ("No error description given");
std::string const Exception::NoDebugGiven_("No debug information given");

Exception::Exception(std::string const & what, std::string const & debug, int const & errNo)
	:what_(Util::strrpl(what, Errno_, "[" + Util::tos(errNo) + "] " + Util::strerror(errNo)))
	,debug_(debug)
	,errNo_(errNo)
{}

Exception::~Exception() throw() {}

char        const * Exception::what()     const throw() { return what_.c_str(); }
std::string         Exception::getDebug() const         { return std::string(std::string(typeid(*this).name()) + ": " + debug_); }
int         const & Exception::getErrno() const         { return errNo_; }

}
