/**
 * @file
 * @brief Include this to use the namespace shortcuts.
 */
#ifndef UI_UTIL_SHORTCUTS_HPP
#define UI_UTIL_SHORTCUTS_HPP

namespace UI { namespace Util { namespace Sys {} namespace Http {} }}

namespace U  = UI::Util;
namespace US = UI::Util::Sys;
namespace UH = UI::Util::Http;

#endif
