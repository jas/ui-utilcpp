/**
 * @file
 */
#ifndef UI_UTIL_CAP_HPP
#define UI_UTIL_CAP_HPP

// STDC++
#include <string>

// POSIX C
#ifdef __linux__
#include <sys/capability.h>
#endif

namespace UI {
namespace Util {

/** @brief C++ encapsulation for libcap's 'cap_t'. */
class Cap
{
private:
#ifdef __linux__
	cap_t cap_;
#endif

public:
	enum InitType { Proc_, Clear_ };
	/** @brief Construct from process state, or cleared with proc=false. */
	Cap(InitType const & initType=Proc_);
	/** @brief Construct from textual representation (see man cap_from_text(3)). */
	Cap(std::string const & s, InitType const & initType=Proc_);
	Cap(Cap const & cap);
	~Cap();

	Cap & apply();

#ifdef HAVE_CAP_COMPARE
	bool operator==(Cap const & cap) const;
#endif

	/** @brief Get textual representation (see man cap_to_text(3)). */
	std::string const get() const;
};

/** @brief Helper to enable effective capabilities safely for a scope. */
class CapScope
{
private:
	std::string const capabilities_;
public:
	CapScope(std::string const & capabilities);
	~CapScope();
};

}}
#endif
