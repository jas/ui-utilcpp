/**
 * @file
 */
#ifndef UI_UTIL_CHARSETMAGIC_HPP
#define UI_UTIL_CHARSETMAGIC_HPP

// STDC++
#include <string>

namespace UI {
namespace Util {
namespace CharsetMagic {

/** @name Guess encoding of data in buffer using len sample size.
 *
 * If no encoding could be guessed, this returns an empty string.
 * @{ */
std::string guess(char const * const buf, size_t const & len);
std::string guess(std::string const & buf, std::string::size_type const & len=std::string::npos);
/** @} */

}}}
#endif
